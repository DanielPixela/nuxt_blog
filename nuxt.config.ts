import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
   css: ['@/assets/sass/app.scss'],
   app: {
     head: {
        titleTemplate: '%s | SCR Zadanie',
        htmlAttrs: {
           lang: 'sk',
        }
     }
   },
   vite: {
      css: {
         preprocessorOptions: {
            scss: {
               additionalData: '@import "@/assets/sass/base/_mixins.scss";@import "@/assets/sass/base/_variables.scss";@import "@/assets/sass/base/_functions.scss";',
            },
         },
      },
   },
})
